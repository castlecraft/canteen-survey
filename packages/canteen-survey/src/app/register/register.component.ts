import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { DURATION, LONG_DURATION } from '../constants/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements AfterViewInit {
  @Input() error: string | null = '';
  @ViewChild('stepper') stepper!: MatStepper;

  emailVerificationCode = '';
  isOtpFieldVisible = false;
  isVerifyPhoneBtnVisible = false;
  isRegisterEmailBtnVisible = false;
  isEmailCodeFieldVisible = false;
  isSendOtpBtnVisible = true;
  isSendOtpBtnDisabled = false;
  isSendEmailBtnVisible = true;
  isSendEmailBtnDisabled = false;
  isVerifyEmailBtnVisible = false;
  isVerifyEmailBtnDisabled = false;
  isRegisterCanteenBtnEnabled = false;
  registerPhoneForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    otp: new FormControl(''),
  });
  registerEmailForm = new FormGroup({
    email: new FormControl('', [Validators.email]),
    emailVerificationCode: new FormControl(''),
  });
  registerCanteenForm = new FormGroup({
    canteenId: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    retypePassword: new FormControl('', [Validators.required]),
  });

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.emailVerificationCode = this.route.snapshot.params.code;
  }

  ngAfterViewInit() {
    if(this.emailVerificationCode) {
      this.registerEmailForm.controls.emailVerificationCode.setValue(this.emailVerificationCode);
      this.verifyEmail();
    }
  }

  registerPhone() {
    const frmCtrls = this.registerPhoneForm.controls;
    this.http.post(
      '/api/survey/register_phone',
      {
        nameOfURCManager: frmCtrls.name.value,
        mobileNumberOfURCManager: frmCtrls.phone.value,
      },
    ).subscribe({
      next: res => {
        this.isOtpFieldVisible = true;
        this.isSendOtpBtnDisabled = true;
        setTimeout(() => this.isSendOtpBtnDisabled = false, LONG_DURATION);
        this.isVerifyPhoneBtnVisible = true;
        frmCtrls.name.disable();
        frmCtrls.phone.disable();
      },
      error: this.handleError.bind(this),
    });
  }

  verifyPhone() {
    const frmCtrls = this.registerPhoneForm.controls;
    this.http.post(
      '/api/survey/verify_phone',
      {
        mobileNumberOfURCManager: frmCtrls.phone.value,
        otp: frmCtrls.otp.value,
      },
    ).subscribe({
      next: res => {
        this.isOtpFieldVisible = false;
        this.isSendOtpBtnVisible = false;
        this.isVerifyPhoneBtnVisible = false;
        this.isRegisterEmailBtnVisible = true;
      },
      error: this.handleError.bind(this),
    });
  }

  registerEmail() {
    const phoneFrm = this.registerPhoneForm.controls;
    const emailFrm = this.registerEmailForm.controls;
    this.http.post(
      '/api/survey/register_email',
      {
        mobileNumberOfURCManager: phoneFrm.phone.value,
        emailOfURCManager: emailFrm.email.value,
      },
    ).subscribe({
      next: res => {
        emailFrm.email.disable();
        this.isEmailCodeFieldVisible = true;
        this.isVerifyEmailBtnVisible = true;
        this.isSendEmailBtnDisabled = true;
        setTimeout(() => this.isSendEmailBtnDisabled = false, LONG_DURATION);
      },
      error: this.handleError.bind(this),
    });
  }

  verifyEmail() {
    const emailFrm = this.registerEmailForm.controls;
    this.http.post<any>(
      '/api/survey/verify_email',
      {
        emailVerificationCode: emailFrm.emailVerificationCode.value,
      },
    ).subscribe({
      next: res => {
        emailFrm.email.disable();
        emailFrm.emailVerificationCode.disable();
        this.isEmailCodeFieldVisible = true;
        this.isRegisterCanteenBtnEnabled = true;
        this.isVerifyEmailBtnDisabled = true;
        this.isSendEmailBtnVisible = false;
        this.isVerifyEmailBtnVisible = false;

        if (this.emailVerificationCode) {
          this.registerPhoneForm.controls.name.setValue(res.nameOfURCManager);
          this.registerPhoneForm.controls.name.disable();
          this.registerPhoneForm.controls.phone.setValue(res.mobileNumberOfURCManager);
          this.registerPhoneForm.controls.phone.disable();
          this.registerEmailForm.controls.email.setValue(res.emailOfURCManager);
          this.registerEmailForm.controls.email.disable();
          this.registerEmailForm.controls.emailVerificationCode.setValue(this.emailVerificationCode);
          this.registerEmailForm.controls.emailVerificationCode.disable();
          this.isSendOtpBtnVisible = false;
          this.isRegisterEmailBtnVisible = true;
          // step to email registration
          this.stepper.next();
          // step to canteen registration
          this.stepper.next();
        }
      },
      error: this.handleError.bind(this),
    });
  }

  registerCanteen(): any {
    const canteenFrm = this.registerCanteenForm.controls;
    if (canteenFrm.password.value !== canteenFrm.retypePassword.value) {
      return this.snackBar.open('Password Mismatch', 'Close');
    }

    const emailFrm = this.registerEmailForm.controls;
    const phoneFrm = this.registerPhoneForm.controls;
    this.http.post(
      '/api/survey/register_canteen',
      {
        mobileNumberOfURCManager: phoneFrm.phone.value,
        emailOfURCManager: emailFrm.email.value,
        canteenId: canteenFrm.canteenId.value,
        password: canteenFrm.password.value,
      },
    ).subscribe({
      next: res => {
        this.router.navigate(['login']);
      },
      error: this.handleError.bind(this),
    });
  }

  handleError(error: any) {
    console.error(error);
    if(Array.isArray(error?.error?.message)) {
      let msg = '';
      error?.error?.message.forEach((err: string) => {
        msg += err + ', ';
      });
      this.snackBar.open(msg, 'Close', { duration: DURATION });
    } else {
      this.snackBar.open(
        error?.error?.message?.toString()
          || error?.message?.toString()
              || error?.toString()
                || 'Something went wrong',
        'Close',
        { duration: DURATION }
      );
    }
    this.router.navigate(['register']);
  }
}
