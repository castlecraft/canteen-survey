import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AppComponent } from './app.component';
import { AppService } from './app.service';

describe('AppComponent', () => {
  let appSvcSpy;
  beforeEach(async () => {
    appSvcSpy = jasmine.createSpyObj(AppService, [
      'addEntry',
      'updateEntry',
      'getEntry',
      'getMessage',
      'getProfile',
      'setInfoLocalStorage',
      'setupOIDC'
    ]);
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: AppService, useValue: appSvcSpy }
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
