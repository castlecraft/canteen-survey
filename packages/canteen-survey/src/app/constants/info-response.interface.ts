export interface InfoResponse {
  [key: string]: any | any[];
}
