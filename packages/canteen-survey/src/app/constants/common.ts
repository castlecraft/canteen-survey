export const NEW_ID = 'new';
export const MODELS = [
  { route: 'client' },
  { route: 'role' },
  { route: 'user' },
  { route: 'scope' },
];
export const DURATION = 5000;
export const UNDO_DURATION = 10000;
export const LONG_DURATION = 20000;
export const SCOPE = 'openid email roles profile phone';
