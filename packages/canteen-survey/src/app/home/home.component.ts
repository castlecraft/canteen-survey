import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  username = '';
  password = '';
  constructor(
    private router: Router,
    private http: HttpClient,
  ) {}

  ngOnInit() {
    this.checkLoggedIn().subscribe({
      next: res => {
        if(res.isLoggedIn) {
          return this.router.navigate(['/survey']);
        }
        return this.router.navigate(['/login']);
      },
      error: error => {
        this.router.navigate(['/login']);
      }
    });
  }

  checkLoggedIn() {
    return this.http.get<{isLoggedIn: boolean}>('/api/survey/is_logged_in');
  }
}
