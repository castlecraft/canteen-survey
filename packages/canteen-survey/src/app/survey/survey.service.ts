import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ISSUER_URL } from '../constants/storage';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  constructor(private readonly http: HttpClient) {}

  addEntry(
    nameOfURCManager: string,
    mobileNumberOfURCManager: string,
    landlineNumberOfURCWithSTDCode: string,
    availableDigitalPaymentMode: string,
    numberOfPOSWorkingInURC: number,
    yearOfInstallationOfPOSMachine: string,
    bankWhoProvidedPOSMachines: string,
    nameOfBankWhereURCHasAccount: string,
    pctOfDigitalPaymentsMadeByBeneficiary: number,
    pctOfCashPaymentsMadeByBeneficiary: number,
    nameOfDependentDepot: string,
  ): Observable<any> {
    return this.http.post<any>('/api/survey/v1/add_entry',
      {
        nameOfURCManager,
        mobileNumberOfURCManager,
        landlineNumberOfURCWithSTDCode,
        availableDigitalPaymentMode,
        numberOfPOSWorkingInURC,
        yearOfInstallationOfPOSMachine,
        bankWhoProvidedPOSMachines,
        nameOfBankWhereURCHasAccount,
        pctOfDigitalPaymentsMadeByBeneficiary,
        pctOfCashPaymentsMadeByBeneficiary,
        nameOfDependentDepot,
      },
    );
  }

  updateEntry(
    nameOfURCManager: string,
    mobileNumberOfURCManager: string,
    landlineNumberOfURCWithSTDCode: string,
    availableDigitalPaymentMode: string,
    numberOfPOSWorkingInURC: number,
    yearOfInstallationOfPOSMachine: string,
    bankWhoProvidedPOSMachines: string,
    nameOfBankWhereURCHasAccount: string,
    pctOfDigitalPaymentsMadeByBeneficiary: number,
    pctOfCashPaymentsMadeByBeneficiary: number,
    nameOfDependentDepot: string,
  ): Observable<any> {
    return this.http.post<any>('/api/survey/v1/update',
      {
        nameOfURCManager,
        mobileNumberOfURCManager,
        landlineNumberOfURCWithSTDCode,
        availableDigitalPaymentMode,
        numberOfPOSWorkingInURC,
        yearOfInstallationOfPOSMachine,
        bankWhoProvidedPOSMachines,
        nameOfBankWhereURCHasAccount,
        pctOfDigitalPaymentsMadeByBeneficiary,
        pctOfCashPaymentsMadeByBeneficiary,
        nameOfDependentDepot,
      },
    );
  }

  getProfile(bearerHeader: string) {
    return this.http.get(localStorage.getItem(ISSUER_URL) + '/oauth2/profile', {
      headers: { authorization: bearerHeader },
    });
  }

  getEntry(phone: string) {
    return this.http.get<any>(
      '/api/survey/v1/fetch_by_phone/' + phone,
    );
  }

  checkLoggedIn() {
    return this.http.get<{
      isLoggedIn: boolean;
      user?: any;
    }>('/api/survey/is_logged_in');
  }
}
