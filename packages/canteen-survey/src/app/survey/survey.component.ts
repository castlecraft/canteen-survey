import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SurveyService } from './survey.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {
  loggedIn = false;
  isAddDisabled = false;
  banks: Array<string> = ['SBI', 'BOB', 'HDFC'];
  depots: Array<string> = ['Depot 1', 'Depot 2', 'Depot 3'];
  years: Array<number> = [];
  paymentMethods: Array<string> = [
    'Visa Card',
    'Master Card',
    'Rupay Card',
    'UPI',
    'Wallet',
    'Net banking',
    'NEFT',
    'Google Pay',
    'Amazon Pay',
    'BHIM',
  ];
  posCountSelect = [1,2,3,4,5,6,7,8,9,10];

  surveyForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*'),
    ]),
    phone: new FormControl('', [Validators.required]),
    landline: new FormControl(''),
    paymentMethods: new FormControl(),
    numberPos: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    years: new FormControl(),
    posBank: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*'),
    ]),
    urcBank: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*'),
    ]),
    digitalPercent: new FormControl('', [
      Validators.required,
      Validators.min(0),
      Validators.max(100),
    ]),
    cashPercent: new FormControl('', [
      Validators.required,
      Validators.min(0),
      Validators.max(100),
    ]),
    depotName: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z 0-9]*'),
    ]),
  });

  constructor(
    private service: SurveyService,
    private snackBar: MatSnackBar,
  ) { }

  addEntry() {
    this.service.addEntry(
      this.surveyForm.controls.name.value,
      this.surveyForm.controls.phone.value,
      this.surveyForm.controls.landline.value,
      this.surveyForm.controls.paymentMethods.value,
      Number(this.surveyForm.controls.numberPos.value),
      this.surveyForm.controls.years.value,
      this.surveyForm.controls.posBank.value,
      this.surveyForm.controls.urcBank.value,
      Number(this.surveyForm.controls.digitalPercent.value),
      Number(this.surveyForm.controls.cashPercent.value),
      this.surveyForm.controls.depotName.value,
    ).subscribe({
      next: res => {
        this.isAddDisabled = true;
        this.snackBar.open('Entry Added Successfully', 'Close', { duration: 10000 });
      },
      error: error => {
        this.snackBar.open(
          error?.error?.message || error?.error?.toString() || error?.toString() || 'Something went wrong',
          'Close',
          { duration: 10000 },
        );
      },
    });
  }

  ngOnInit() {
    this.service.checkLoggedIn().subscribe({
      next: res => {
        this.loggedIn = res.isLoggedIn;
        this.setSessionInfo(res.user);
      },
      error: error => this.loggedIn = false,
    });
    this.loadLastTenYears();
  }

  setSessionInfo(user: any) {
    if (user && user.mobileNumberOfURCManager) {
      this.surveyForm.controls.phone.setValue(user.mobileNumberOfURCManager);
      this.loadEntryByPhone(this.surveyForm.controls.phone.value);
    }
  }

  loadLastTenYears() {
    const year = new Date().getFullYear();
    this.years.push(year);
    for (let i = 1; i <= 10; i++) {
      this.years.push(year - i);
    }
  }

  loadEntryByPhone(phone?: string) {
    if(phone) {
      this.service.getEntry(phone).subscribe({
        next: res => {
          this.surveyForm.controls.name.setValue(res.nameOfURCManager);
          this.surveyForm.controls.paymentMethods.setValue(res.availableDigitalPaymentMode);
          this.surveyForm.controls.posBank.setValue(res.bankWhoProvidedPOSMachines);
          this.surveyForm.controls.landline.setValue(res.landlineNumberOfURCWithSTDCode);
          this.surveyForm.controls.phone.setValue(res.mobileNumberOfURCManager);
          this.surveyForm.controls.urcBank.setValue(res.nameOfBankWhereURCHasAccount);
          this.surveyForm.controls.depotName.setValue(res.nameOfDependentDepot);
          this.surveyForm.controls.numberPos.setValue(res.numberOfPOSWorkingInURC);
          this.surveyForm.controls.cashPercent.setValue(res.pctOfCashPaymentsMadeByBeneficiary);
          this.surveyForm.controls.digitalPercent.setValue(res.pctOfDigitalPaymentsMadeByBeneficiary);
          this.surveyForm.controls.years.setValue(res.yearOfInstallationOfPOSMachine);
          this.isAddDisabled = true;
        },
        error: () => {
          this.isAddDisabled = false;
        },
      });
    }
  }

  updateEntry() {
    this.service.updateEntry(
      this.surveyForm.controls.name.value,
      this.surveyForm.controls.phone.value,
      this.surveyForm.controls.landline.value ? this.surveyForm.controls.landline.value : undefined,
      this.surveyForm.controls.paymentMethods.value,
      Number(this.surveyForm.controls.numberPos.value),
      this.surveyForm.controls.years.value,
      this.surveyForm.controls.posBank.value,
      this.surveyForm.controls.urcBank.value,
      Number(this.surveyForm.controls.digitalPercent.value),
      Number(this.surveyForm.controls.cashPercent.value),
      this.surveyForm.controls.depotName.value,
    ).subscribe({
      next: res => {
        this.isAddDisabled = true;
        this.snackBar.open('Entry Updated Successfully', 'Close', { duration: 10000 });
      },
      error: error => {
        this.snackBar.open(
          error?.error?.message || error?.error?.toString() || error?.toString() || 'Something went wrong',
          'Close',
          { duration: 10000 },
        );
      },
    });
  }
}
