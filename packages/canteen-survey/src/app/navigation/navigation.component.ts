import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavigationService } from './navigation.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isLoggedIn = false;
  avatar = '';

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private navigationService: NavigationService,
  ) {}

  ngOnInit(): void {
    this.watchRouteChange();
  }

  logout() {
    window.location.href = '/api/survey/logout';
  }

  watchRouteChange() {
    this.router.events.subscribe(value => {
      this.checkSession();
    });
  }

  checkSession() {
    this.navigationService.checkLoggedIn().subscribe({
      next: res => {
        if(res.isLoggedIn) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      },
      error: error => {
        this.isLoggedIn = false;
      }
    });
  }
}
