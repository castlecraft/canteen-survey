import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NavigationService {
  constructor(private readonly http: HttpClient) {}
  checkLoggedIn() {
    return this.http.get<{isLoggedIn: boolean}>('/api/survey/is_logged_in');
  }
}
