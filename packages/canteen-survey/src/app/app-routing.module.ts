import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './constants/auth.guard.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SurveyComponent } from './survey/survey.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },

  // ADD ROUTES BELOW THIS
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'register/:code', component: RegisterComponent },
  { path: 'survey', component: SurveyComponent, canActivate: [AuthGuard] },

  // ADD ROUTES ABOVE THIS
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
