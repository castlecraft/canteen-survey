import { HttpClient } from '@angular/common/http';
import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() error: string | null = '';

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
  ){}

  ngOnInit() {
    this.checkLoggedIn().subscribe({
      next: res => {
        if(res.isLoggedIn) {
          this.router.navigate(['/survey']);
        }
      },
      error: console.error,
    });
  }

  checkLoggedIn() {
    return this.http.get<{isLoggedIn: boolean}>('/api/survey/is_logged_in');
  }

  submit() {
    if (this.form.valid) {
      this.http.post(
        '/api/survey/login',
        {
          username: this.form.controls.username.value,
          password: this.form.controls.password.value,
        },
      ).subscribe({
        next: success => {
          this.router.navigate(['/survey']);
        },
        error: error => {
          console.error(error);
          this.error = 'Invalid Credentials';
        },
      });
    }
  }
}
