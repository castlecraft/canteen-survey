import * as Joi from '@hapi/joi';
import * as dotenv from 'dotenv';
import { Injectable } from '@nestjs/common';

export interface EnvConfig {
  [prop: string]: string;
}

export const NODE_ENV = 'NODE_ENV';
export const DB_NAME = 'DB_NAME';
export const DB_HOST = 'DB_HOST';
export const DB_USER = 'DB_USER';
export const DB_PASSWORD = 'DB_PASSWORD';
export const MONGO_URI_PREFIX = 'MONGO_URI_PREFIX';
export const EVENTS_HOST = 'EVENTS_HOST';
export const EVENTS_PROTO = 'EVENTS_PROTO';
export const EVENTS_PORT = 'EVENTS_PORT';
export const EVENTS_USER = 'EVENTS_USER';
export const EVENTS_PASSWORD = 'EVENTS_PASSWORD';
export const EMAIL_HOST = 'EMAIL_HOST';
export const EMAIL_PORT = 'EMAIL_PORT';
export const EMAIL_USERNAME = 'EMAIL_USERNAME';
export const EMAIL_PASSWORD = 'EMAIL_PASSWORD';
export const EMAIL_FROM = 'EMAIL_FROM';
export const TWILIO_SID = 'TWILIO_SID';
export const TWILIO_AUTH_TOKEN = 'TWILIO_AUTH_TOKEN';
export const TWILIO_NUMBER = 'TWILIO_NUMBER';
@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.config().parsed;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      [NODE_ENV]: Joi.string()
        .valid('development', 'production', 'test', 'provision', 'staging')
        .default('development'),
      [DB_NAME]: Joi.string().required(),
      [DB_HOST]: Joi.string().required(),
      [DB_USER]: Joi.string().required(),
      [DB_PASSWORD]: Joi.string().required(),
      [EMAIL_HOST]: Joi.string().required(),
      [EMAIL_PORT]: Joi.string().required(),
      [EMAIL_USERNAME]: Joi.string().required(),
      [EMAIL_PASSWORD]: Joi.string().required(),
      [EMAIL_FROM]: Joi.string().required(),
      [TWILIO_SID]: Joi.string().required(),
      [TWILIO_AUTH_TOKEN]: Joi.string().required(),
      [TWILIO_NUMBER]: Joi.string().required(),
      [MONGO_URI_PREFIX]: Joi.string().optional(),
      [EVENTS_PROTO]: Joi.string().optional(),
      [EVENTS_USER]: Joi.string().optional(),
      [EVENTS_PASSWORD]: Joi.string().optional(),
      [EVENTS_HOST]: Joi.string().optional(),
      [EVENTS_PORT]: Joi.string().optional(),
    });

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
