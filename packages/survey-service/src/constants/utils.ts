export function generateOTP() {
  let OTP = '';
  for (let i = 0; i < 6; i++) {
    OTP += '0123456789'[Math.floor(Math.random() * 10)];
  }
  return padWithZeros(Number(OTP), 6);
}

export function padWithZeros(number: number, length: number) {
  let paddedString = '' + number;
  while (paddedString.length < length) {
    paddedString = '0' + paddedString;
  }

  return paddedString;
}
