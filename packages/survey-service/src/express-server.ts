import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as passport from 'passport';
import * as helmet from 'helmet';
import { INestApplication } from '@nestjs/common';
import { MongoStore } from 'connect-mongo';
import { SessionOptions } from 'express-session';

import { ConfigService, NODE_ENV } from './config/config.service';
import { SESSION_CONNECTION } from './common/mongo-session.store';

export class ExpressServer {
  public server: express.Express;

  constructor(private config: ConfigService) {
    this.server = express();
  }

  setupSecurity() {
    // Helmet
    this.server.use(helmet());

    // Enable Trust Proxy for session to work
    // https://github.com/expressjs/session/issues/281
    this.server.set('trust proxy', 1);
  }

  setupSession(app: INestApplication) {
    const secret = process.env.SESSION_SECRET || 'changeit';
    this.server.use(cookieParser(secret));

    const cookie = {
      maxAge: 7.884e9,
      httpOnly: false,
      secure: true,
    };

    if (this.config.get(NODE_ENV) !== 'production') {
      cookie.secure = false;
    }
    const { store, expressSession } = app.get<{
      store: MongoStore;
      expressSession: (options?: SessionOptions) => express.RequestHandler;
    }>(SESSION_CONNECTION);

    const sessionConfig = {
      name: 'survey-io',
      secret,
      store,
      cookie,
      saveUninitialized: false,
      resave: false,
      proxy: true, // https://github.com/expressjs/session/issues/281
      // unset: 'destroy'
    };

    this.server.use(expressSession(sessionConfig));
    this.server.use(passport.initialize());
    this.server.use(passport.session());
  }
}
