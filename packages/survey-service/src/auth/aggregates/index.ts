import { ConnectedDeviceService } from './connected-device/connected-device.service';
import { ClientTokenManagerService } from './client-token-manager/client-token-manager.service';
import { LocalStrategy } from './local-strategy/local.strategy';
import { CookieSerializer } from './local-strategy/passport-cookie.serializer';

export const AuthAggregates = [
  CookieSerializer,
  LocalStrategy,
  ConnectedDeviceService,
  ClientTokenManagerService,
];
