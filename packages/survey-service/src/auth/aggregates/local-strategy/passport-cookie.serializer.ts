import { Injectable } from '@nestjs/common';
import * as passport from 'passport';

@Injectable()
export class CookieSerializer {
  constructor() {
    passport.serializeUser((user, done) => this.serializeUser(user, done));
    passport.deserializeUser((payload, done) =>
      this.deserializeUser(payload, done),
    );
  }

  serializeUser(user: any, done: (err, user) => any) {
    // add user into array of users for multi-user per session?
    done(null, {
      canteenId: user.canteenId,
      uuid: user.uuid,
      mobileNumberOfURCManager: user.mobileNumberOfURCManager,
      nameOfURCManager: user.nameOfURCManager,
      emailOfURCManager: user.emailOfURCManager,
      isPhoneVerified: user.isPhoneVerified,
      isEmailVerified: user.isEmailVerified,
      isVerified: user.isVerified,
    });
  }
  deserializeUser(payload: any, done: (err, payload) => any): any {
    // remove user from users for logout
    done(null, payload);
  }
}
