import { Strategy } from 'passport-local';
import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { SurveyService } from '../../../survey/entities/survey/survey.service';
// import { PassportStrategy } from '@nestjs/passport';
import { PassportStrategy } from './passport.strategy';
import { createHash } from 'crypto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private survey: SurveyService) {
    super();
  }

  async validate(canteenId: string, password: string): Promise<any> {
    const user = await this.survey.findOne({ canteenId });
    const hashedPassword = createHash('md5').update(password).digest('hex');
    if (!user) {
      throw new UnauthorizedException();
    } else if (user.password !== hashedPassword) {
      throw new ForbiddenException();
    }
    return user;
  }
}

export const callback = (err, user, info) => {
  if (typeof info !== 'undefined') {
    throw new UnauthorizedException(info.message);
  } else if (err || !user) {
    throw err || new UnauthorizedException();
  }
  return user;
};
