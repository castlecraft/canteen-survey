import { UnauthorizedException } from '@nestjs/common';
import * as passport from 'passport';

export const createPassportContext = (request, response) => (types, options) =>
  new Promise((resolve, reject) =>
    passport.authenticate(types, options, (err, user, info) => {
      try {
        if (options.passReqToCallback) {
          return resolve(options.callback(request, err, user, info));
        } else {
          return resolve(options.callback(err, user, info));
        }
      } catch (error) {
        reject(error);
      }
    })(request, response, resolve),
  );

export const defaultOptions = {
  session: false,
  property: 'user',
  callback: (err, user, info) => {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  },
};

export interface RequestUser {
  email: string;
  uuid: string;
  phone: string;
  disabled: boolean;
  enable2fa: boolean;
  enablePasswordLess: boolean;
  roles: string[];
}
