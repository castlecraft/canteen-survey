import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import { switchMap, retry } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';
import * as Express from 'express';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import { TokenCache } from '../../auth/entities/token-cache/token-cache.entity';
import { TokenCacheService } from '../../auth/entities/token-cache/token-cache.service';
import { TOKEN } from '../../constants/app-strings';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(
    private readonly settingsService: ServerSettingsService,
    private readonly tokenCacheService: TokenCacheService,
    private readonly http: HttpService,
  ) {}
  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const accessToken = this.getAccessToken(req);
    return from(this.tokenCacheService.findOne({ accessToken })).pipe(
      switchMap(cachedToken => {
        if (!cachedToken) {
          return this.introspectToken(accessToken, req);
        } else if (Math.floor(new Date().getTime() / 1000) < cachedToken.exp) {
          req[TOKEN] = cachedToken;
          return of(true);
        } else {
          from(
            this.tokenCacheService.deleteMany({
              accessToken: cachedToken.accessToken,
            }),
          ).subscribe({
            next: removed => {},
            error: error => {},
          });

          return this.introspectToken(accessToken, req);
        }
      }),
    );
  }

  introspectToken(accessToken: string, req: Express.Request) {
    return from(this.settingsService.find()).pipe(
      switchMap(settings => {
        if (!settings) {
          return throwError(new NotImplementedException());
        }
        const baseEncodedCred = Buffer.from(
          settings.clientId + ':' + settings.clientSecret,
        ).toString('base64');
        return this.http
          .post(
            settings.introspectionURL,
            { token: accessToken },
            { headers: { Authorization: 'Basic ' + baseEncodedCred } },
          )
          .pipe(
            retry(3),
            switchMap(response => {
              if (response.data.active) {
                return from(this.cacheToken(response.data, accessToken)).pipe(
                  switchMap(cachedToken => {
                    req[TOKEN] = cachedToken;
                    return of(cachedToken.active);
                  }),
                );
              }
              return of(false);
            }),
          );
      }),
    );
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }

  cacheToken(introspectedToken, accessToken: string): Promise<TokenCache> {
    const token = new TokenCache();
    token.accessToken = accessToken;
    token.clientId = introspectedToken.client_id;
    token.trustedClient = introspectedToken.trusted_client;
    token.phoneNumber = introspectedToken.phone_number;
    return this.tokenCacheService.save(introspectedToken);
  }
}
