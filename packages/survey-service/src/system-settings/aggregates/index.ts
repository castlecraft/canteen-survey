import { SettingsService } from './settings/settings.service';
import { HealthCheckAggregateService } from './health-check/health-check.service';
import { ClientTokenManagerService } from './client-token-manager/client-token-manager.service';

export const SystemSettingsAggregates = [
  SettingsService,
  HealthCheckAggregateService,
  ClientTokenManagerService,
];
