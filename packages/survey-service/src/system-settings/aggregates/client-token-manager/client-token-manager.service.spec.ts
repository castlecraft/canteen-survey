import { Test, TestingModule } from '@nestjs/testing';
import { ClientTokenManagerService } from './client-token-manager.service';
import { HttpService } from '@nestjs/common';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { SettingsService } from '../settings/settings.service';

describe('ClientTokenManagerService', () => {
  let service: ClientTokenManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientTokenManagerService,
        { provide: TokenCacheService, useValue: {} },
        { provide: SettingsService, useValue: {} },
        { provide: HttpService, useValue: {} },
      ],
    }).compile();

    service = module.get<ClientTokenManagerService>(ClientTokenManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
