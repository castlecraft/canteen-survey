import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { setupSwagger } from './swagger';
import { GLOBAL_API_PREFIX } from './constants/app-strings';
import { ConfigService } from './config/config.service';
import { ExpressServer } from './express-server';

async function bootstrap() {
  const authServer = new ExpressServer(new ConfigService());
  const server = new ExpressAdapter(authServer.server);
  const app = await NestFactory.create(AppModule, server);

  app.enableCors();
  app.setGlobalPrefix(GLOBAL_API_PREFIX);
  setupSwagger(app);
  authServer.setupSecurity();
  authServer.setupSession(app);
  await app.listen(8000);
}
bootstrap();
