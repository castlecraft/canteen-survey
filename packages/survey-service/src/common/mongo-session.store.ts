import { Logger } from '@nestjs/common';
import * as connectMongoDBSession from 'connect-mongo';
import { Observable } from 'rxjs';
import { retryWhen, scan, delay } from 'rxjs/operators';
import * as expressSession from 'express-session';

import {
  ConfigService,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_USER,
  MONGO_URI_PREFIX,
} from '../config/config.service';

export const SESSION_CONNECTION = 'SESSION_CONNECTION';

export const databaseProviders = [
  {
    provide: SESSION_CONNECTION,
    useFactory: async (config: ConfigService) => {
      const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';
      const MongoStore = connectMongoDBSession(expressSession);
      const mongoOptions = 'retryWrites=true';
      const store = new MongoStore({
        url: `${mongoUriPrefix}://${config.get(DB_USER)}:${config.get(
          DB_PASSWORD,
        )}@${config.get(DB_HOST).replace(/,\s*$/, '')}/${config.get(
          DB_NAME,
        )}?${mongoOptions}`,
        touchAfter: 24 * 3600, // 24 hours * 3600 secs
        collection: 'express_session',
        stringify: false,
        mongoOptions: {
          useUnifiedTopology: true,
          // w: MAJORITY,
          useNewUrlParser: true,
        },
      });
      return {
        store,
        expressSession,
      };
    },
    inject: [ConfigService],
  },
];

export function handleRetry(
  retryAttempts = 9,
  retryDelay = 3000,
): <T>(source: Observable<T>) => Observable<T> {
  return <T>(source: Observable<T>) =>
    source.pipe(
      retryWhen(e =>
        e.pipe(
          scan((errorCount, error) => {
            Logger.error(
              `Unable to connect to the database. Retrying (${
                errorCount + 1
              })...`,
              '',
              'DatabaseProvider',
            );
            if (errorCount + 1 >= retryAttempts) {
              throw error;
            }
            return errorCount + 1;
          }, 0),
          delay(retryDelay),
        ),
      ),
    );
}
