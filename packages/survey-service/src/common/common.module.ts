import { Global, HttpModule, Module } from '@nestjs/common';
import { databaseProviders } from './mongo-session.store';

@Global()
@Module({
  imports: [HttpModule],
  providers: [...databaseProviders],
  exports: [HttpModule, ...databaseProviders],
})
export class CommonModule {}
