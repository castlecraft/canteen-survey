import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Canteen } from './canteen.entity';

@Injectable()
export class CanteenService {
  constructor(
    @InjectRepository(Canteen)
    private readonly repo: MongoRepository<Canteen>,
  ) {}

  async save(params) {
    const canteen = new Canteen();
    Object.assign(canteen, params);
    return await this.repo.save(canteen);
  }

  async find(params?): Promise<Canteen[]> {
    return await this.repo.find(params);
  }

  async findOne(params) {
    return await this.repo.findOne(params);
  }

  async update(query, params) {
    return await this.repo.update(query, params);
  }

  async count() {
    return this.repo.count();
  }

  async delete(params?: unknown) {
    return this.repo.delete(params);
  }
}
