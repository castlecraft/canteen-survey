import {
  Column,
  Entity,
  BaseEntity,
  ObjectID,
  ObjectIdColumn,
  Index,
} from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Canteen extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  @Index({ sparse: true, unique: true })
  canteenId: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
