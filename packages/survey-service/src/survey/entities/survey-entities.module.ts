import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Canteen } from './canteen/canteen.entity';
import { CanteenService } from './canteen/canteen.service';
import { Survey } from './survey/survey.entity';
import { SurveyService } from './survey/survey.service';

@Module({
  imports: [TypeOrmModule.forFeature([Survey, Canteen])],
  providers: [SurveyService, CanteenService],
  exports: [SurveyService, CanteenService],
})
export class SurveyEntitiesModule {}
