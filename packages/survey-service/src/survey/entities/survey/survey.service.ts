import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Survey } from './survey.entity';

@Injectable()
export class SurveyService {
  constructor(
    @InjectRepository(Survey)
    private readonly surveyRepository: MongoRepository<Survey>,
  ) {}

  async save(params) {
    const survey = new Survey();
    Object.assign(survey, params);
    return await this.surveyRepository.save(survey);
  }

  async find(): Promise<Survey> {
    const settings = await this.surveyRepository.find();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.surveyRepository.findOne(params);
  }

  async update(query, params) {
    return await this.surveyRepository.updateOne(query, params);
  }

  async count() {
    return this.surveyRepository.count();
  }

  async delete(params?: unknown) {
    return this.surveyRepository.delete(params);
  }
}
