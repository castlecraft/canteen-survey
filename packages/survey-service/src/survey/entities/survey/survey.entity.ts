import {
  Column,
  Entity,
  BaseEntity,
  ObjectID,
  ObjectIdColumn,
  Index,
} from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Survey extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  nameOfURCManager: string;

  @Column()
  @Index({ unique: true, sparse: true })
  mobileNumberOfURCManager: string;

  @Column()
  @Index({ unique: true, sparse: true })
  emailOfURCManager: string;

  @Column()
  @Index({ unique: true, sparse: true })
  canteenId: string;

  @Column()
  landlineNumberOfURCWithSTDCode: string;

  @Column()
  availableDigitalPaymentMode: string;

  @Column()
  numberOfPOSWorkingInURC: number;

  @Column()
  yearOfInstallationOfPOSMachine: string;

  @Column()
  bankWhoProvidedPOSMachines: string;

  @Column()
  nameOfBankWhereURCHasAccount: string;

  @Column()
  pctOfDigitalPaymentsMadeByBeneficiary: string;

  @Column()
  pctOfCashPaymentsMadeByBeneficiary: string;

  @Column()
  nameOfDependentDepot: string;

  @Column()
  @Index({ unique: true, sparse: true })
  emailVerificationCode: string;

  @Column()
  @Index({ unique: true, sparse: true })
  phoneVerificationCode: string;

  @Column()
  password: string;

  @Column()
  isVerified: boolean = false;

  @Column()
  isPhoneVerified: boolean = false;

  @Column()
  isEmailVerified: boolean = false;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
