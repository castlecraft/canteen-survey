import { IsEmail, IsNotEmpty } from 'class-validator';
import { IsMobileE164 } from '../../../auth/decorators/is-mobile-e164.decorator';

export class RegistrationEmailDTO {
  @IsMobileE164()
  @IsNotEmpty()
  mobileNumberOfURCManager: string;

  @IsEmail()
  @IsNotEmpty()
  emailOfURCManager: string;
}
