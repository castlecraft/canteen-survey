import { IsEmail, IsNotEmpty } from 'class-validator';

export class EmailMessageDto {
  @IsEmail()
  emailTo: string;

  @IsNotEmpty()
  subject: string;

  @IsNotEmpty()
  text: string;

  @IsNotEmpty()
  html: string;
}
