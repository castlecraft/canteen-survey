import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { IsMobileE164 } from '../../../auth/decorators/is-mobile-e164.decorator';

export class RegistrationCanteenDTO {
  @IsMobileE164()
  @IsNotEmpty()
  mobileNumberOfURCManager: string;

  @IsEmail()
  @IsNotEmpty()
  emailOfURCManager: string;

  @IsString()
  @IsNotEmpty()
  canteenId: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
