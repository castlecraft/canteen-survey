import {
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class UpdateSurveyDTO {
  @IsPhoneNumber('IN')
  @IsOptional()
  landlineNumberOfURCWithSTDCode: string;

  @IsString({ each: true })
  availableDigitalPaymentMode: string[];

  @IsNumber()
  numberOfPOSWorkingInURC: number;

  @IsNumber()
  yearOfInstallationOfPOSMachine: string;

  @IsString()
  bankWhoProvidedPOSMachines: string;

  @IsString()
  nameOfBankWhereURCHasAccount: string;

  @IsNumber()
  @Max(100)
  @Min(0)
  pctOfDigitalPaymentsMadeByBeneficiary: string;

  @IsNumber()
  @Max(100)
  @Min(0)
  pctOfCashPaymentsMadeByBeneficiary: string;

  @IsString()
  nameOfDependentDepot: string;
}
