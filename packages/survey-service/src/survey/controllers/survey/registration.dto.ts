import { IsNotEmpty } from 'class-validator';
import { IsMobileE164 } from '../../../auth/decorators/is-mobile-e164.decorator';
import { IsFullName } from '../../../auth/decorators/is-full-name.decorator';

export class RegistrationDTO {
  @IsFullName()
  @IsNotEmpty()
  nameOfURCManager: string;

  @IsMobileE164()
  @IsNotEmpty()
  mobileNumberOfURCManager: string;
}
