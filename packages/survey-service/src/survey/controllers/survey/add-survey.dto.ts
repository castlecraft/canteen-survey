import {
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { IsMobileE164 } from '../../../auth/decorators/is-mobile-e164.decorator';
import { IsFullName } from '../../../auth/decorators/is-full-name.decorator';

export class AddSurveyDTO {
  @IsFullName()
  nameOfURCManager: string;

  @IsMobileE164()
  mobileNumberOfURCManager: string;

  @IsOptional()
  @IsPhoneNumber('IN')
  landlineNumberOfURCWithSTDCode: string;

  @IsString({ each: true })
  availableDigitalPaymentMode: string[];

  @IsNumber()
  numberOfPOSWorkingInURC: number;

  @IsNumber()
  yearOfInstallationOfPOSMachine: string;

  @IsString()
  bankWhoProvidedPOSMachines: string;

  @IsString()
  nameOfBankWhereURCHasAccount: string;

  @IsNumber()
  @Max(100)
  @Min(0)
  pctOfDigitalPaymentsMadeByBeneficiary: string;

  @IsNumber()
  @Max(100)
  @Min(0)
  pctOfCashPaymentsMadeByBeneficiary: string;

  @IsString()
  nameOfDependentDepot: string;
}
