import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  Res,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Request } from 'express';
import { SurveyAggregateService } from '../../aggregates/survey-aggregate/survey-aggregate.service';
import { AddSurveyDTO } from './add-survey.dto';
import { RegistrationDTO } from './registration.dto';
import { UpdateSurveyDTO } from './update-survey.dto';
import { LocalUserGuard } from '../../../auth/guards/local-user.guard';
import { EnsureLoginGuard } from '../../../auth/guards/ensure-login.guard';
import { Survey } from '../../entities/survey/survey.entity';
import { VerificationDTO } from './verification.dto';
import { RegistrationEmailDTO } from './registration-email.dto';
import { VerificationEmailDTO } from './verification-email.dto';
import { RegistrationCanteenDTO } from './registration-canteen.dto';

@Controller('survey')
export class SurveyController {
  constructor(private readonly aggregate: SurveyAggregateService) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalUserGuard)
  login(@Req() req, @Res() res) {
    if (req.query && req.query.redirect) {
      return res.redirect(req.query.redirect);
    }
    return res.json({ canteenId: req.user?.canteenId });
  }

  @Get('logout')
  logout(@Req() req, @Res() res) {
    req.logout && req.logout();
    if (req.query && req.query.redirect) {
      return res.redirect(req.query.redirect);
    }
    return res.redirect('/');
  }

  @Post('register_phone')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async register(@Body() payload: RegistrationDTO) {
    return await this.aggregate.registerMobile(payload);
  }

  @Post('verify_phone')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async verifyPhone(@Body() payload: VerificationDTO) {
    const { mobileNumberOfURCManager, otp } = payload;
    return await this.aggregate.verifyMobile(mobileNumberOfURCManager, otp);
  }

  @Post('register_email')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async registerEmail(@Body() payload: RegistrationEmailDTO) {
    return await this.aggregate.registerEmail(payload);
  }

  @Post('verify_email')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async verifyEmail(@Body() payload: VerificationEmailDTO) {
    const { emailVerificationCode } = payload;
    return await this.aggregate.verifyEmail(emailVerificationCode);
  }

  @Post('register_canteen')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async registerCanteen(@Body() payload: RegistrationCanteenDTO) {
    const {
      mobileNumberOfURCManager,
      emailOfURCManager,
      canteenId,
      password,
    } = payload;
    return await this.aggregate.registerCanteen(
      mobileNumberOfURCManager,
      emailOfURCManager,
      canteenId,
      password,
    );
  }

  @Get('is_logged_in')
  async isLoggedIn(@Req() req) {
    const user = req.user;
    return {
      isLoggedIn: (req.isAuthenticated && req.isAuthenticated()) || false,
      user: {
        canteenId: user?.canteenId,
        uuid: user?.uuid,
        mobileNumberOfURCManager: user?.mobileNumberOfURCManager,
        nameOfURCManager: user?.nameOfURCManager,
        emailOfURCManager: user?.emailOfURCManager,
        phoneVerified: user?.phoneVerificationCode ? true : false,
        emailVerified: user?.emailVerificationCode ? true : false,
      },
    };
  }

  @Get('v1/fetch/:uuid')
  @UseGuards(EnsureLoginGuard)
  async fetchSurveyByUUID(
    @Param('uuid') uuid: string,
    @Req() req: Request & { user: Survey },
  ) {
    return await this.aggregate.fetchSurveyByUUID(
      uuid,
      req?.user?.mobileNumberOfURCManager,
    );
  }

  @Get('v1/fetch_by_phone/:phone')
  @UseGuards(EnsureLoginGuard)
  async fetchSurveyByPhone(
    @Param('phone') phone: string,
    @Req() req: Request & { user: Survey },
  ) {
    return await this.aggregate.fetchSurveyByPhone(
      phone,
      req?.user?.mobileNumberOfURCManager,
    );
  }

  @Post('v1/add_entry')
  @UseGuards(EnsureLoginGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addSurvey(
    @Body() payload: AddSurveyDTO,
    @Req() req: Request & { user: Survey },
  ) {
    return await this.aggregate.addSurvey(
      payload,
      req?.user?.mobileNumberOfURCManager,
    );
  }

  @Post('v1/delete/:uuid')
  @UseGuards(EnsureLoginGuard)
  async deleteSurvey(
    @Param('uuid') uuid: string,
    @Req() req: Request & { user: Survey },
  ) {
    return await this.aggregate.deleteSurvey(uuid, req?.user);
  }

  @Post('v1/update')
  @UseGuards(EnsureLoginGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async updateSurvey(
    @Body() payload: UpdateSurveyDTO,
    @Req() req: Request & { user: any },
  ) {
    return await this.aggregate.updateSurvey(payload, req?.user);
  }
}
