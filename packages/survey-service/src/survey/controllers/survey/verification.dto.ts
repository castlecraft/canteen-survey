import { IsNotEmpty, IsString } from 'class-validator';
import { IsMobileE164 } from '../../../auth/decorators/is-mobile-e164.decorator';

export class VerificationDTO {
  @IsMobileE164()
  @IsNotEmpty()
  mobileNumberOfURCManager: string;

  @IsString()
  otp: string;
}
