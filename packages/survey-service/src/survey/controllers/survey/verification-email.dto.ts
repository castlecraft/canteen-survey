import { IsString } from 'class-validator';

export class VerificationEmailDTO {
  @IsString()
  emailVerificationCode: string;
}
