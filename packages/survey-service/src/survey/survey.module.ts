import { Global, Module } from '@nestjs/common';
import { MessengerService } from './aggregates/messenger/messenger.service';
import { SendEmailService } from './aggregates/send-email/send-email.service';
import { SurveyAggregateService } from './aggregates/survey-aggregate/survey-aggregate.service';
import { SurveyController } from './controllers/survey/survey.controller';
import { SurveyEntitiesModule } from './entities/survey-entities.module';

@Global()
@Module({
  imports: [SurveyEntitiesModule],
  controllers: [SurveyController],
  providers: [SurveyAggregateService, SendEmailService, MessengerService],
  exports: [SurveyEntitiesModule],
})
export class SurveyModule {}
