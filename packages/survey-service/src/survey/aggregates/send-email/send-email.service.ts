import { Injectable, Logger } from '@nestjs/common';
import {
  ConfigService,
  EMAIL_FROM,
  EMAIL_HOST,
  EMAIL_PASSWORD,
  EMAIL_PORT,
  EMAIL_USERNAME,
} from '../../../config/config.service';
import * as nodemailer from 'nodemailer';
import { EmailMessageDto } from '../../controllers/survey/email-message-dto';

@Injectable()
export class SendEmailService {
  constructor(private readonly config: ConfigService) {}

  async processMessage(payload: EmailMessageDto) {
    const from = this.config.get(EMAIL_FROM);
    const message = {
      from,
      to: payload.emailTo,
      subject: payload.subject,
      text: payload.text,
      html: payload.html,
    };

    const transport = await this.getSMTPTransport();
    transport
      .sendMail(message)
      .then(async success => {
        Logger.log('Email Sent', this.constructor.name);
      })
      .catch(async error => {
        Logger.error(error, error.toString(), this.constructor.name);
      });
  }

  async getSMTPTransport() {
    const host = this.config.get(EMAIL_HOST);
    const port = this.config.get(EMAIL_PORT);
    const user = this.config.get(EMAIL_USERNAME);
    const pass = this.config.get(EMAIL_PASSWORD);
    return nodemailer.createTransport({
      host,
      port,
      auth: { user, pass },
    });
  }
}
