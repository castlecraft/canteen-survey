import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '../../../config/config.service';
import { MessengerService } from '../../aggregates/messenger/messenger.service';
import { SendEmailService } from '../../aggregates/send-email/send-email.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { SurveyService } from '../../entities/survey/survey.service';
import { SurveyAggregateService } from './survey-aggregate.service';
import { CanteenService } from '../../entities/canteen/canteen.service';

describe('SurveyAggregateService', () => {
  let service: SurveyAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SurveyAggregateService,
        { provide: SendEmailService, useValue: {} },
        { provide: MessengerService, useValue: {} },
        { provide: SettingsService, useValue: {} },
        { provide: ConfigService, useValue: {} },
        { provide: SurveyService, useValue: {} },
        { provide: CanteenService, useValue: {} },
      ],
    }).compile();

    service = module.get<SurveyAggregateService>(SurveyAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
