import {
  ForbiddenException,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { createHash, randomBytes } from 'crypto';
import { RegistrationDTO } from '../../controllers/survey/registration.dto';
import { SurveyService } from '../../entities/survey/survey.service';
import { Survey } from '../../entities/survey/survey.entity';
import { generateOTP } from '../../../constants/utils';
import { SendEmailService } from '../send-email/send-email.service';
import { MessengerService } from '../messenger/messenger.service';
import { EmailMessageDto } from '../../controllers/survey/email-message-dto';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ConfigService, NODE_ENV } from '../../../config/config.service';
import { RegistrationEmailDTO } from '../../controllers/survey/registration-email.dto';
import { CanteenService } from '../../entities/canteen/canteen.service';

@Injectable()
export class SurveyAggregateService {
  constructor(
    private readonly survey: SurveyService,
    private readonly email: SendEmailService,
    private readonly sms: MessengerService,
    private readonly settings: SettingsService,
    private readonly config: ConfigService,
    private readonly canteen: CanteenService,
  ) {}

  async registerMobile(payload: RegistrationDTO) {
    const { nameOfURCManager, mobileNumberOfURCManager } = payload;
    const byPhone = await this.survey.findOne({ mobileNumberOfURCManager });
    if (byPhone && byPhone.isVerified) {
      throw new UnauthorizedException('Mobile already registered');
    }

    const survey = await this.survey.save({
      ...(byPhone || {}),
      ...payload,
      nameOfURCManager,
      phoneVerificationCode: generateOTP(),
    });

    const message = `Phone verification OTP ${survey.phoneVerificationCode}.\nDo not share with anyone.`;
    const nodEnv = this.config.get(NODE_ENV);
    if (nodEnv === 'development') {
      Logger.log(
        {
          message,
          phone: survey.mobileNumberOfURCManager,
        },
        this.constructor.name,
      );
    } else {
      Logger.log(
        {
          message,
          phone: survey.mobileNumberOfURCManager,
        },
        this.constructor.name,
      );
      await this.sendSMS(message, survey.mobileNumberOfURCManager);
    }
    survey.phoneVerificationCode = undefined;
    survey.emailVerificationCode = undefined;
    survey._id = undefined;
    return survey;
  }

  async verifyMobile(mobileNumberOfURCManager: string, otp: string) {
    const survey = await this.survey.findOne({ mobileNumberOfURCManager });
    if (
      survey &&
      survey.phoneVerificationCode &&
      survey.phoneVerificationCode === otp
    ) {
      survey.isPhoneVerified = true;
      survey.phoneVerificationCode = undefined;
      await this.survey.update({ uuid: survey.uuid }, { $set: survey });
      await this.survey.update(
        { uuid: survey.uuid },
        { $unset: { phoneVerificationCode: 1 } },
      );
      return survey;
    }

    if (survey?.phoneVerificationCode !== otp) {
      throw new ForbiddenException('Invalid OTP');
    }
    throw new UnauthorizedException('Invalid Mobile');
  }

  async registerEmail(payload: RegistrationEmailDTO) {
    const { mobileNumberOfURCManager, emailOfURCManager } = payload;
    const byPhone = await this.survey.findOne({ mobileNumberOfURCManager });
    if (!byPhone) {
      throw new NotFoundException({ mobileNumberOfURCManager });
    }
    if (byPhone && byPhone.emailOfURCManager && byPhone.isVerified) {
      throw new UnauthorizedException('Mobile already registered');
    }
    const byEmail = await this.survey.findOne({ emailOfURCManager });
    if (byEmail && byEmail.isVerified) {
      throw new UnauthorizedException('Email already registered');
    }

    const survey = {
      ...byPhone,
      ...payload,
      emailOfURCManager,
      emailVerificationCode: randomBytes(32).toString('hex'),
    } as Survey;

    const settings = await this.settings.find().toPromise();
    const verificationUrl =
      settings.appURL + '/register/' + survey.emailVerificationCode;
    const emailPayload = {
      emailTo: survey.emailOfURCManager,
      subject: 'Email Verification for Survey',
      text:
        'Visit the following link to complete email verification\n' +
        verificationUrl +
        '\nOr use code ' +
        survey.emailVerificationCode,
      html: `To complete verification <a href='${verificationUrl}'>click here</a> or use code ${survey.emailVerificationCode}`,
    };
    const nodEnv = this.config.get(NODE_ENV);

    if (nodEnv === 'development') {
      Logger.log(emailPayload, this.constructor.name);
    } else {
      Logger.log(emailPayload, this.constructor.name);
      await this.sendEmail(emailPayload);
    }
    await this.survey.update({ uuid: survey.uuid }, { $set: survey });
    survey.emailVerificationCode = undefined;
    return survey;
  }

  async verifyEmail(emailVerificationCode: string) {
    const survey = await this.survey.findOne({
      emailVerificationCode,
    });
    if (!survey) {
      throw new NotFoundException('Invalid Email Verification Code');
    }
    survey.emailVerificationCode = undefined;
    survey.isEmailVerified = true;
    await this.survey.update({ uuid: survey.uuid }, { $set: survey });
    await this.survey.update(
      { uuid: survey.uuid },
      { $unset: { emailVerificationCode: 1 } },
    );
    return survey;
  }

  async registerCanteen(
    mobileNumberOfURCManager: string,
    emailOfURCManager: string,
    canteenId: string,
    password: string,
  ) {
    const byCanteenId = await this.survey.findOne({ canteenId });
    if (byCanteenId) {
      throw new UnauthorizedException('Canteen ID already registered');
    }

    const survey = await this.survey.findOne({
      emailOfURCManager,
      mobileNumberOfURCManager,
    });
    if (!survey) {
      throw new NotFoundException({
        emailOfURCManager,
        mobileNumberOfURCManager,
      });
    }
    if (survey.isEmailVerified && survey.isPhoneVerified) {
      const canteen = await this.canteen.findOne({ canteenId });
      if (!canteen) {
        throw new ForbiddenException('Invalid Canteen ID');
      }
      survey.canteenId = canteenId;
      survey.isVerified = true;
      survey.password = createHash('md5').update(password).digest('hex');
      await this.survey.update({ uuid: survey.uuid }, { $set: survey });
      survey.password = undefined;
      return survey;
    }
    throw new UnauthorizedException('Email and Phone not verified');
  }

  async fetchSurveyByUUID(uuid: string, phone: string) {
    const survey = await this.survey.findOne({ uuid });
    if (!survey) {
      throw new NotFoundException({ uuid });
    }

    if (survey.mobileNumberOfURCManager !== phone) {
      throw new ForbiddenException({ InvalidUser: phone });
    }

    return survey;
  }

  async fetchSurveyByPhone(mobileNumberOfURCManager: string, phone: string) {
    const survey = await this.survey.findOne({ mobileNumberOfURCManager });
    if (!survey) {
      throw new NotFoundException({ mobileNumberOfURCManager });
    }

    if (survey.mobileNumberOfURCManager !== phone) {
      throw new ForbiddenException({ InvalidUser: phone });
    }

    return survey;
  }

  async deleteSurvey(uuid: string, token: Survey) {
    const survey = await this.survey.findOne({ uuid });
    if (token.mobileNumberOfURCManager === survey.mobileNumberOfURCManager) {
      return await this.survey.delete({ uuid });
    }

    throw new ForbiddenException({ phone: token.mobileNumberOfURCManager });
  }

  async addSurvey(payload: unknown, mobileNumberOfURCManager: string) {
    const existingSurvey = await this.survey.findOne({
      mobileNumberOfURCManager,
    });
    if (existingSurvey) {
      throw new UnauthorizedException({ existingSurvey });
    }
    return await this.survey.save(payload);
  }

  async updateSurvey(payload: unknown, token: Survey) {
    const mobileNumberOfURCManager = token?.mobileNumberOfURCManager;
    const existingSurvey = await this.survey.findOne({
      mobileNumberOfURCManager,
    });
    if (
      existingSurvey &&
      existingSurvey.mobileNumberOfURCManager !== mobileNumberOfURCManager
    ) {
      throw new UnauthorizedException({
        NotAllowedToUpdate: mobileNumberOfURCManager,
      });
    }

    Object.assign(existingSurvey, payload);
    return await this.survey.update(
      { uuid: existingSurvey.uuid },
      { $set: existingSurvey },
    );
  }

  async sendEmail(payload: EmailMessageDto) {
    await this.email.processMessage(payload);
  }

  sendSMS(body: string, to: string) {
    this.sms.sendMessage(body, to);
  }
}
