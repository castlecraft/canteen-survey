import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import {
  ConfigService,
  TWILIO_AUTH_TOKEN,
  TWILIO_NUMBER,
  TWILIO_SID,
} from '../../../config/config.service';
import * as twilio from 'twilio';
import { Twilio } from 'twilio';

@Injectable()
export class MessengerService implements OnModuleInit {
  client: Twilio;

  constructor(private readonly config: ConfigService) {}

  onModuleInit() {
    this.client = twilio(
      this.config.get(TWILIO_SID),
      this.config.get(TWILIO_AUTH_TOKEN),
    );
  }

  async sendMessage(body: string, to: string) {
    const from = this.config.get(TWILIO_NUMBER);
    return await this.client.messages.create({ body, to, from });
  }

  async sendPhoneVerificationMessage(otp: string, expiry: Date, phone: string) {
    let message = `Phone verification OTP ${otp}.`;
    message += `Code expires on ${expiry}. Do not share with anyone.`;
    try {
      await this.sendMessage(message, phone);
    } catch (error) {
      Logger.error(error, error?.toString(), this.constructor.name);
    }
  }
}
